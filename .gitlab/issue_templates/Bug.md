<!-- Please include a one sentence summary of the bug as the issue title. Just typing "help" or "[this] doesn't work" doesn't help anyone, including yourself. -->

<!-- Remember: NOBODY CAN READ YOUR MIND OR SEE YOUR SCREEN WHEN YOU POST AN ISSUE! Please put all information you think is relevant in *this initial post*. It's better to provide too much information than not enough information. -->

### Checklist
* [ ] I'm on the latest version/commit of neonbt++
* [ ] I can reproduce the bug every time I want it to happen
* I have searched both open and closed issues, and:
    * [ ] mine hasn't been submitted before
    * [ ] mine is similar to (an) issue(s) that has/have been submitted before, but I think this one is different <!-- Please explain if so, and provide links to such issue(s) -->

### Summary
<!-- Type a slightly longer summary. -->

### Minimal Reproducible Example
<!-- Provide the smallest amount of code that reproduces the bug. -->

### Expected Behavior
<!-- What do you expect neonbt++ to do? -->

### Actual Behavior
<!-- What did neonbt++ actually do? -->

### Proposed Solution
<!-- If you know, how do you think this bug can be fixed? -->

### System Info
<!-- Put your compiler, OS, and neonbt++ version here. -->
