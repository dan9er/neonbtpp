<!-- Please include a one sentence summary of the enhancement as the issue title. Just typing "I have an idea" or "make [this] better" isn't very convincing. -->

### How will this improve neonbt++?
<!-- ... -->

### How is this in neonbt++'s scope? 
<!-- Is it not a better job for another library or program? -->

### Does implementing it require linking/adding support for additional libraries?
<!-- ... -->
