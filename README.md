# neonbt++
neonbt++ is a free/libre C++11 library for reading, manipulating, and writing data in Mojang's Named Binary Tag (NBT) format.

neonbt++ is a fork of [ilja-ag's libnbt++ v2.5](https://github.com/ljfa-ag/libnbtplusplus/tree/v2.5), seeking to fix issues and improve upon it further.

## Changes/Improvements over libnbt++2
### Breaking
- Include statements and file structure are fixed to conform to library conventions. (Not exactly breaking, since they were broken in the first place.)
- zlib related classes have been *removed entirely* to reduce the project's scope. (Instead of those, include and use [zstr](https://github.com/mateidavid/zstr) streams.)

### Non-Breaking
- Install target has been added.
- Tests have been reworked to be portable.
- Some expressions are replaced with equivalent modern versions (eg. `typename` -> `using =`).

## Thanks
neonbt++ incorprates some code from other libnbt++ forks:
- [GitHub/AlexAndDad:installer](https://github.com/AlexAndDad/libnbtplusplus/tree/installer)
- [GitHub/MultiMC:master](https://github.com/MultiMC/libnbtplusplus)

The commits from these repos have been merged into this one.

## License
**neonbt++ *is not* an official Minecraft product; and *is not* approved by or associated with Mojang, Xbox Game Studios, or Microsoft.**

neonbt++ is licensed under GNU LGPL v3. See [COPYING](COPYING) and [COPYING.LESSER](COPYING.LESSER) for terms.
