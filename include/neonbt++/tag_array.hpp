/*
 * neonbt++
 * Copyright (C) 2013, 2015  ljfa-ag
 * Copyright (C) 2020  dan9er & contributors
 *
 * This file is part of neonbt++.
 *
 * neonbt++ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * neonbt++ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with neonbt++.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <type_traits>
#include <vector>

#include <neonbt++/crtp_tag.hpp>

namespace nbt
{

///@cond
namespace detail
{
    ///Meta-struct that holds the tag_type value for a specific array type
    template<class T> struct get_array_type
    { static_assert(sizeof(T) != sizeof(T), "Invalid type parameter for tag_array, can only use byte or int"); };

    template<> struct get_array_type<int8_t>  : public std::integral_constant<tag_type, tag_type::Byte_Array> {};
    template<> struct get_array_type<int32_t> : public std::integral_constant<tag_type, tag_type::Int_Array> {};
    template<> struct get_array_type<int64_t> : public std::integral_constant<tag_type, tag_type::Long_Array> {};
}
///@endcond

/**
 * @brief Tag that contains an array of byte, int or long values
 *
 * Common class for tag_byte_array, tag_int_array and tag_long_array.
 */
template<class T>
class tag_array final : public detail::crtp_tag<tag_array<T>>
{
public:
    //Iterator types
    using iterator = typename std::vector<T>::iterator;
    using const_iterator = typename std::vector<T>::const_iterator;

    ///The type of the contained values
    using value_type = T;

    ///The type of the tag
    static constexpr tag_type type = detail::get_array_type<T>::value;

    ///Constructs an empty array
    tag_array() {}

    ///Constructs an array with the given values
    tag_array(std::initializer_list<T> init): data(init) {}
    tag_array(std::vector<T>&& vec) noexcept: data(std::move(vec)) {}

    ///Returns a reference to the vector that contains the values
    std::vector<T>& get() { return data; }
    const std::vector<T>& get() const { return data; }

    /**
     * @brief Accesses a value by index with bounds checking
     * @throw std::out_of_range if the index is out of range
     */
    T& at(size_t i) { return data.at(i); }
    T at(size_t i) const { return data.at(i); }

    /**
     * @brief Accesses a value by index
     *
     * No bounds checking is performed.
     */
    T& operator[](size_t i) { return data[i]; }
    T operator[](size_t i) const { return data[i]; }

    ///Appends a value at the end of the array
    void push_back(T val) { data.push_back(val); }

    ///Removes the last element from the array
    void pop_back() { data.pop_back(); }

    ///Returns the number of values in the array
    size_t size() const { return data.size(); }

    ///Erases all values from the array.
    void clear() { data.clear(); }

    //Iterators
    iterator begin() { return data.begin(); }
    iterator end()   { return data.end(); }
    const_iterator begin() const  { return data.begin(); }
    const_iterator end() const    { return data.end(); }
    const_iterator cbegin() const { return data.cbegin(); }
    const_iterator cend() const   { return data.cend(); }

    void read_payload(io::stream_reader& reader) override;
    /**
     * @inheritdoc
     * @throw std::length_error if the array is too large for NBT
     */
    void write_payload(io::stream_writer& writer) const override;

private:
    std::vector<T> data;
};

template<class T> bool operator==(const tag_array<T>& lhs, const tag_array<T>& rhs)
{ return lhs.get() == rhs.get(); }
template<class T> bool operator!=(const tag_array<T>& lhs, const tag_array<T>& rhs)
{ return !(lhs == rhs); }

//Typedefs that should be used instead of the template tag_array.
using tag_byte_array = tag_array<int8_t>;
using tag_int_array  = tag_array<int32_t>;
using tag_long_array = tag_array<int64_t>;

//Explicit instantiations
template class NBT_EXPORT tag_array<int8_t>;
template class NBT_EXPORT tag_array<int32_t>;
template class NBT_EXPORT tag_array<int64_t>;
template <>
void tag_array<int64_t>::read_payload(io::stream_reader&);
template <>
void tag_array<int64_t>::write_payload(io::stream_writer&) const;

}
