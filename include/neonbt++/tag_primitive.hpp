/*
 * neonbt++
 * Copyright (C) 2013, 2015  ljfa-ag
 * Copyright (C) 2020  dan9er & contributors
 *
 * This file is part of neonbt++.
 *
 * neonbt++ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * neonbt++ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with neonbt++.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <istream>
#include <sstream>

#include <neonbt++/crtp_tag.hpp>
#include <neonbt++/primitive_detail.hpp>
#include <neonbt++/io/stream_reader.hpp>
#include <neonbt++/io/stream_writer.hpp>

namespace nbt
{

/**
 * @brief Tag that contains an integral or floating-point value
 *
 * Common class for tag_byte, tag_short, tag_int, tag_long, tag_float and tag_double.
 */
template<class T>
class tag_primitive final : public detail::crtp_tag<tag_primitive<T>>
{
public:
    ///The type of the value
    using value_type = T;

    ///The type of the tag
    static constexpr tag_type type = detail::get_primitive_type<T>::value;

    //Constructor
    constexpr tag_primitive(T val = 0) noexcept: value(val) {}

    //Getters
    operator T&() { return value; }
    constexpr operator T() const { return value; }
    constexpr T get() const { return value; }

    //Setters
    tag_primitive& operator=(T val) { value = val; return *this; }
    void set(T val) { value = val; }

    void read_payload(io::stream_reader& reader) override;
    void write_payload(io::stream_writer& writer) const override;

private:
    T value;
};

template<class T> bool operator==(const tag_primitive<T>& lhs, const tag_primitive<T>& rhs)
{ return lhs.get() == rhs.get(); }
template<class T> bool operator!=(const tag_primitive<T>& lhs, const tag_primitive<T>& rhs)
{ return !(lhs == rhs); }

//Typedefs that should be used instead of the template tag_primitive.
using tag_byte   = tag_primitive<int8_t>;
using tag_short  = tag_primitive<int16_t>;
using tag_int    = tag_primitive<int32_t>;
using tag_long   = tag_primitive<int64_t>;
using tag_float  = tag_primitive<float>;
using tag_double = tag_primitive<double>;

//Explicit instantiations
template class NBT_EXPORT tag_primitive<int8_t>;
template class NBT_EXPORT tag_primitive<int16_t>;
template class NBT_EXPORT tag_primitive<int32_t>;
template class NBT_EXPORT tag_primitive<int64_t>;
template class NBT_EXPORT tag_primitive<float>;
template class NBT_EXPORT tag_primitive<double>;

template<class T>
void tag_primitive<T>::read_payload(io::stream_reader& reader)
{
    reader.read_num(value);
    if(!reader.get_istr())
    {
        std::ostringstream str;
        str << "Error reading tag_" << type;
        throw io::input_error(str.str());
    }
}

template<class T>
void tag_primitive<T>::write_payload(io::stream_writer& writer) const
{
    writer.write_num(value);
}

}
