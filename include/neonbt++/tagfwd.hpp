/*
 * neonbt++
 * Copyright (C) 2013, 2015  ljfa-ag
 * Copyright (C) 2020  dan9er & contributors
 *
 * This file is part of neonbt++.
 *
 * neonbt++ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * neonbt++ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with neonbt++.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file
 * @brief Provides forward declarations for all tag classes
 */
#pragma once

#include <cstdint>

namespace nbt
{

class tag;

template<class T> class tag_primitive;
using tag_byte   = tag_primitive<int8_t>;
using tag_short  = tag_primitive<int16_t>;
using tag_int    = tag_primitive<int32_t>;
using tag_long   = tag_primitive<int64_t>;
using tag_float  = tag_primitive<float>;
using tag_double = tag_primitive<double>;

class tag_string;

template<class T> class tag_array;
using tag_byte_array = tag_array<int8_t>;
using tag_int_array  = tag_array<int32_t>;
using tag_long_array = tag_array<int64_t>;

class tag_list;
class tag_compound;

}
