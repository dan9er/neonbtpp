/*
 * neonbt++
 * Copyright (C) 2013, 2015  ljfa-ag
 * Copyright (C) 2020  dan9er & contributors
 *
 * This file is part of neonbt++.
 *
 * neonbt++ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * neonbt++ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with neonbt++.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ostream>

#include <neonbt++/tagfwd.hpp>
#include <neonbt++/nbt_export.hpp>

namespace nbt
{
namespace text
{

/**
 * @brief Prints tags in a JSON-like syntax into a stream
 *
 * @todo Make it configurable and able to produce actual standard-conformant JSON
 */
class NBT_EXPORT json_formatter
{
public:
    json_formatter() {}
    void print(std::ostream& os, const tag& t) const;
};

}
}
