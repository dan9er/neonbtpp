# neonbt++
# Copyright (C) 2013, 2015  ljfa-ag
# Copyright (C) 2020  dan9er & contributors
#
# This file is part of neonbt++.
#
# neonbt++ is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# neonbt++ is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with neonbt++.  If not, see <http://www.gnu.org/licenses/>.

# enable testing and find CxxTest
enable_testing()
find_package(CxxTest REQUIRED)

# include directories
include_directories(${${NEONBT_NAME}_SOURCE_DIR}/include)
include_directories(${${NEONBT_NAME}_BINARY_DIR}/generated/include)
include_directories(${CXXTEST_INCLUDE_DIR})
include_directories(${CMAKE_CURRENT_BINARY_DIR})

# add zstr (& zlib) if enabled
if(NEONBT_TEST_WITH_ZSTR)
    find_path(ZSTR_INCLUDE_DIRS "zstr/zstr.hpp" REQUIRED)
    include_directories(${ZSTR_INCLUDE_DIRS})

    find_package(ZLIB REQUIRED)
    set(EXTRA_TEST_LIBS ZLIB::ZLIB)
endif()

# configure a header file to pass some of the CMake settings to the source code
configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/cmake_vars.hpp.in
    ${CMAKE_CURRENT_BINARY_DIR}/cmake_vars.hpp
)

# copy binary data
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/data DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

# helper function that silences some warnings
function(stop_warnings target)
    if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
        target_compile_options(${target} PRIVATE
            -Wno-unused-value
            -Wno-self-assign-overloaded
            -Wno-language-extension-token
        )
    endif()
    # TODO add GCC and MSVC
endfunction()

#########
# TESTS #
#########

CXXTEST_ADD_TEST(nbttest nbttest.cpp ${CMAKE_CURRENT_SOURCE_DIR}/nbttest.hpp)
target_link_libraries(nbttest ${NEONBT_NAME})
stop_warnings(nbttest)

CXXTEST_ADD_TEST(endian_str_test endian_str_test.cpp ${CMAKE_CURRENT_SOURCE_DIR}/endian_str_test.hpp)
target_link_libraries(endian_str_test ${NEONBT_NAME})
stop_warnings(endian_str_test)

CXXTEST_ADD_TEST(read_test read_test.cpp ${CMAKE_CURRENT_SOURCE_DIR}/read_test.hpp)
target_link_libraries(read_test ${NEONBT_NAME} ${EXTRA_TEST_LIBS})
stop_warnings(read_test)

CXXTEST_ADD_TEST(write_test write_test.cpp ${CMAKE_CURRENT_SOURCE_DIR}/write_test.hpp)
target_link_libraries(write_test ${NEONBT_NAME} ${EXTRA_TEST_LIBS})
stop_warnings(write_test)

add_executable(format_test format_test.cpp)
target_link_libraries(format_test ${NEONBT_NAME})
add_test(format_test format_test)
stop_warnings(format_test)